---
title: "Week 7"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
# KiCad Tutorial
* [Introduction](https://www.youtube.com/watch?v=BVhWh3AsXQs&amp;list=PLy2022BX6EspFAKBCgRuEuzapuz_4aJCn&amp;index=1)
* [Schematic Symbol creation](https://www.youtube.com/watch?v=LaUd8WfFooU&amp;list=PLy2022BX6EspFAKBCgRuEuzapuz_4aJCn&amp;index=2)
* [Making the Schematic](https://www.youtube.com/watch?v=01OYONrYtNQ&amp;list=PLy2022BX6EspFAKBCgRuEuzapuz_4aJCn&amp;index=3)
* [Associating Symbol and Footprint](https://www.youtube.com/watch?v=IIPKGoW0VBY&amp;list=PLy2022BX6EspFAKBCgRuEuzapuz_4aJCn&amp;index=4)
* [Creating a Custom Footprint](https://www.youtube.com/watch?v=-7yFlz6wdUA&amp;list=PLy2022BX6EspFAKBCgRuEuzapuz_4aJCn&amp;index=5)
* [Finishing the Layout](https://www.youtube.com/watch?v=D_7Y33AoagY&amp;list=PLy2022BX6EspFAKBCgRuEuzapuz_4aJCn&amp;index=6)
* [Output files for Manufacturing](https://www.youtube.com/watch?v=3oL6ZdCUZvA&amp;list=PLy2022BX6EspFAKBCgRuEuzapuz_4aJCn&amp;index=7)




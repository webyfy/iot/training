---
title: "Week 2"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
Basic Electronics
  * [Introduction](https://www.youtube.com/watch?v=s3vpH3A_eTA&amp;ab_channel=SimplyInfo)
  * [Why learn it?](https://www.youtube.com/watch?v=zyuRcsM0gjI&amp;ab_channel=EEVblog)
  * [Reference Material](https://drive.google.com/file/d/1yyD-03sGD6rNr-FBsdnx2KLKHf6Se-W7/view?usp=sharing)
  
Basics of electricity
  * [Electric circuit](https://learn.sparkfun.com/tutorials/what-is-a-circuit)
  * [AC and DC](https://learn.sparkfun.com/tutorials/alternating-current-ac-vs-direct-current-dc)

Passive components
  * [Resistors](https://www.youtube.com/watch?v=Gc1wVdbVI0E&amp;ab_channel=ResistorGuide.com)
  * [Capacitors](https://www.youtube.com/watch?v=X4EUwTwZ110)
  * [Inductors](https://www.youtube.com/watch?v=KSylo01n5FY)
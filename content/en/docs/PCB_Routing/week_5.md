---
title: "Week 5"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
* [Analog v/s digital](https://learn.sparkfun.com/tutorials/analog-vs-digital)
* [Digital logic](https://learn.sparkfun.com/tutorials/digital-logic)
* [Analog to digital conversion](https://learn.sparkfun.com/tutorials/analog-to-digital-conversion)
* [Logic levels](https://learn.sparkfun.com/tutorials/logic-levels)



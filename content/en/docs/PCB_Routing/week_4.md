---
title: "Week 4"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---

  * [Integrated circuits](https://www.youtube.com/watch?v=sTwRQDVHNiw)
    * [Microcontrollers](https://www.sparkfun.com/tutorials/57)
      * [Arduino](https://www.youtube.com/watch?v=nL34zDTPkcs)
      * [Raspberry pi](https://www.youtube.com/watch?v=EKPobkb1N6o)
    * [ASICs](https://www.youtube.com/watch?v=ccKj8kGFhUg&amp;ab_channel=FLEmbedded)
    * [Linear integrated circuits](https://drive.google.com/file/d/1yyD-03sGD6rNr-FBsdnx2KLKHf6Se-W7/view?usp=sharing)
  * [Operational amplifiers](https://www.youtube.com/watch?v=kqCV-HGJc6A)
  * [Opto electronic components](https://maker.pro/custom/tutorial/intro-to-electronic-components-optoelectronics)






---
title: "Week 3"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
  
Electromechanical components
  * [Switches](https://www.youtube.com/watch?v=AbqGWNIItTo)
  
Electromagnetic devices
  * [Relays](https://maker.pro/custom/tutorial/intro-to-electronic-components-relays)
  * [Speakers and microphones](https://www.youtube.com/watch?v=RlajQKES7WA&amp;ab_channel=element14presents)
  * [Motors](https://www.youtube.com/watch?v=CWulQ1ZSE3c)
  
Active components
  * [Transistors](https://www.youtube.com/watch?v=J4oO7PT_nzQ)



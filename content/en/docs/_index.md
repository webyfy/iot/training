---
title: "Guidelines"
description: "test post index"
date: 2020-01-11T14:09:21+09:00
draft: false
---

# Guidelines
* The training program is scheduled to happen over __2 months__
* The training program is split into __3 major topics__. Student may __learn any number of topics__
* Students shall cover the subtopics listed in the program in order. They may use the __given links or refer other  sources__ for the same.
* Each week there shall be an online doubt clearance session if requested by the students
* At the end of the training session there shall be a __test and an interview__ of the students
* Enclosure design students shall avail the student license for Autodesk Inventor from their college. 
* The details of the hands-on session shall be updated considering the covid situation.
* If any suggestions or queries arises please contact the team leads in your college.

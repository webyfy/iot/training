---
title: "Week 1"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
# Version Control System
* [What is Version Control?](https://www.youtube.com/watch?v=9GKpbI1siow)
* [Git stage, commit, push, pull](https://www.youtube.com/watch?v=n-p1RUmdl9M)
* [Git branching and merging](https://www.youtube.com/watch?v=hufGg2mf7eA)
* [Merge conflict resolution](https://www.youtube.com/watch?v=JtIX3HJKwfo)




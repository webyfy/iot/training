---
title: "Week 3"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 3
---
# Advanced
* [Understanding frameworks](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks)
* [Understanding web APIs](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs)
* [Memory Management](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Memory_Management)
* [web server](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_web_server)
* [server-side website programming](https://developer.mozilla.org/en-US/docs/Learn/Server-side/First_steps/Introduction#what_is_server-side_website_programming)
## Node
* [Introduction](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Introduction)
* [Node development environment](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment)
## Reference for Web server
* [Reference Tutorial](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Tutorial_local_library_website#in_this_module)

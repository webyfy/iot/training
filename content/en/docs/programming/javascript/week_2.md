---
title: "Week 2"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
# Basics
* [Promises](https://www.learn-js.org/en/Promises)
* [Async and Await](https://www.learn-js.org/en/Async_and_Await)
* [Object Oriented JavaScript](https://www.learn-js.org/en/Object_Oriented_JavaScript)
* [Function Context](https://www.learn-js.org/en/Function_Context)
* [Inheritance](https://www.learn-js.org/en/Inheritance)
* [Destructuring](https://www.learn-js.org/en/Destructuring)
  
# Intermediate
* [A re-introduction to JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript)
* [JavaScript data structures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)
* [Equality comparisons and sameness](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness)
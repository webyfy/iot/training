---
title: "Week 1"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---

JavaScript (JS) is a lightweight, interpreted, or [just-in-time](https://en.wikipedia.org/wiki/Just-in-time_compilation) compiled programming language with [first-class functions](https://developer.mozilla.org/en-US/docs/Glossary/First-class_Function). While it is most well-known as the scripting language for Web pages and many more. [Read more](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
# Basics
> May practice on Visual Studio Code
* [Hello World!](https://www.learn-js.org/en/Hello%2C_World%21)
* [Variables and Types](https://www.learn-js.org/en/Variables_and_Types)
* [Arrays](https://www.learn-js.org/en/Arrays)
* [Manipulating Arrays](https://www.learn-js.org/en/Manipulating_Arrays)
* [Operators](https://www.learn-js.org/en/Operators)
* [Conditions](https://www.learn-js.org/en/Conditions)
* [Loops](https://www.learn-js.org/en/Loops)
* [Objects](https://www.learn-js.org/en/Objects)
* [Functions](https://www.learn-js.org/en/Functions)
* [Pop ups](https://www.learn-js.org/en/Pop-up_Boxes)
* [Callbacks](https://www.learn-js.org/en/Callbacks)
* [Arrow Functions](https://www.learn-js.org/en/Arrow_Functions)
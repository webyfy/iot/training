---
title: "Week 4"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
# Basics of classes in C++
* [Classes and objects](https://www.w3schools.com/cpp/cpp_classes.asp)
* [Methods](https://www.w3schools.com/cpp/cpp_class_methods.asp)
* [Constructors](https://www.w3schools.com/cpp/cpp_constructors.asp)
* [Access specifiers](https://www.w3schools.com/cpp/cpp_access_specifiers.asp)
* [Encapsulation](https://www.w3schools.com/cpp/cpp_encapsulation.asp)


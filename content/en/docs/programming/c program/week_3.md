---
title: "Week 3"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
# C programming basics __(cont.)__
* [Functions](https://www.learn-c.org/en/Functions)
* [Static](https://www.learn-c.org/en/Static)
* [Pointers](https://www.learn-c.org/en/Pointers)
* [Structures](https://www.learn-c.org/en/Structures)
* [Function arguments by reference](https://www.learn-c.org/en/Function_arguments_by_reference)
* [Dynamic allocation](https://www.learn-c.org/en/Dynamic_allocation)
* [Arrays and pointers](https://www.learn-c.org/en/Arrays_and_Pointers)
* [Recursions](https://www.learn-c.org/en/Recursion)


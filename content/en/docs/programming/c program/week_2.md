---
title: "Week 2"
date: 2021-05-06T13:15:55+05:30
draft: false
weight: 1
---
# C programming basics
> May practice on Visual Studio Code
* [Hello World!](https://www.learn-c.org/en/Hello%2C_World%21)
* [Variables and Types](https://www.learn-c.org/en/Variables_and_Types)
* [Arrays](https://www.learn-c.org/en/Arrays)
* [Multi Dimensional Arrays](https://www.learn-c.org/en/Multidimensional_Arrays)
* [Conditions](https://www.learn-c.org/en/Conditions)
* [Strings](https://www.learn-c.org/en/Strings)
* [For loops](https://www.learn-c.org/en/For_loops)
* [While loops](https://www.learn-c.org/en/While_loops)

